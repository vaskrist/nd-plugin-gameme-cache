new const String:cacheFilePath[64] = "data/gameme_cache.txt";

/// Caches the stats by steam_id
CacheToFile()
{
	new Handle:kv = GetFileCache();
	if (kv != INVALID_HANDLE)
	{
		for (new client = 0; client < MaxClients; client++)
		{
			if (infoCache[client][valid])
			{
				KvRewind(kv);
				if (KvJumpToKey(kv, infoCache[client][steam_id], true))
				{
					WriteStatsToKv(kv, infoCache[client]);
				}
			}
		}
		
		StoreFileCache(kv);
		CloseHandle(kv);
		LogMsg(Format(logMsg, sizeof(logMsg), "Records successfully saved to cache."));
	}
}

/// Read from cache based on steam_id
bool:ReadFromCacheFile(client)
{
	new bool:found = false;
	
	new String:steamId[64];
	GetUniqueId(client, steamId, sizeof(steamId));
	
	new Handle:kv = GetFileCache();
	if (kv != INVALID_HANDLE)
	{
		if (KvJumpToKey(kv, steamId))
		{
			ReadStatsFromKv(kv, infoCache[client]);
			UpdateStats(infoCache[client], client);
			MarkCacheValid(client, true);
			found = true;
		}
		CloseHandle(kv);
	}
	
	if (!found)
	{
		LogMsg(Format(logMsg, sizeof(logMsg), "Client %d not found in cache", client));
	}
	
	return found;
}

/// Gets or creates file cache
Handle:GetFileCache()
{
	new String:filePath[64];
	BuildPath(Path_SM, filePath, sizeof(filePath), cacheFilePath);
	
	new Handle:kv = CreateKeyValues("gameme_cache");
	if (!FileToKeyValues(kv, filePath))
	{
		LogMsg(Format(logMsg, sizeof(logMsg), "Creating new cache file in %s", filePath));
		KeyValuesToFile(kv, filePath);
		KvRewind(kv);
		
		if (!FileToKeyValues(kv, filePath))
		{
			LogErr(Format(logMsg, sizeof(logMsg), "Could not create cache file in %s", filePath));
			return INVALID_HANDLE;
		}
	}
	
	return kv;
}

/// Saves file cache to disk
StoreFileCache(Handle:kv)
{
	if (kv != INVALID_HANDLE)
	{
		new String:filePath[64];
		BuildPath(Path_SM, filePath, sizeof(filePath), cacheFilePath);
		
		KvRewind(kv);
		KeyValuesToFile(kv, filePath);
		LogMsg(Format(logMsg, sizeof(logMsg), "Cache saved to disk: %s.", filePath));
	}
}

/// Reads an item from the file cache
ReadStatsFromKv(Handle:kv, stats[PlayerStat])
{
	// custom non gameMe values here
	KvGetString(kv, "steam_id", stats[steam_id], sizeof(stats[steam_id]));
	stats[data_timestamp] = KvGetNum  (kv, "data_timestamp");
	
	// current values
	stats[rank]            = KvGetNum  (kv, "rank");
	stats[players]         = KvGetNum  (kv, "players");	
	stats[skill]           = KvGetNum  (kv, "skill");	
	stats[kills]           = KvGetNum  (kv, "kills");	
	stats[deaths]          = KvGetNum  (kv, "deaths");	
	stats[kpd]             = KvGetFloat(kv, "kpd");
	stats[suicides]        = KvGetNum  (kv, "suicides");
	stats[headshots]       = KvGetNum  (kv, "headshots");
	stats[hpk]             = KvGetFloat(kv, "hpk");
	stats[accuracy]        = KvGetFloat(kv, "accuracy");
	stats[connection_time] = KvGetNum  (kv, "connection_time");
	stats[kill_assists]    = KvGetNum  (kv, "kill_assists");
	stats[kills_assisted]  = KvGetNum  (kv, "kills_assisted");
	stats[points_healed]   = KvGetNum  (kv, "points_healed");
	stats[flags_captured]  = KvGetNum  (kv, "flags_captured");
	stats[custom_wins]     = KvGetNum  (kv, "custom_wins");
	stats[kill_streak]     = KvGetNum  (kv, "kill_streak");
	stats[death_streak]    = KvGetNum  (kv, "death_streak");
	
	// session values
	stats[session_pos_change]     = KvGetNum  (kv, "session_pos_change");
	stats[session_skill_change]   = KvGetNum  (kv, "session_skill_change");
	stats[session_kills]          = KvGetNum  (kv, "session_kills");
	stats[session_deaths]         = KvGetNum  (kv, "session_deaths");
	stats[session_kpd]            = KvGetFloat(kv, "session_kpd");
	stats[session_suicides]       = KvGetNum  (kv, "session_suicides");
	stats[session_headshots]      = KvGetNum  (kv, "session_headshots");
	stats[session_hpk] 		      = KvGetFloat(kv, "session_hpk");
	stats[session_accuracy]       = KvGetFloat(kv, "session_accuracy");
	stats[session_time]           = KvGetNum  (kv, "session_time");
	stats[session_kill_assists]   = KvGetNum  (kv, "session_kill_assists");
	stats[session_kills_assisted] = KvGetNum  (kv, "session_kills_assisted");
	stats[session_points_healed]  = KvGetNum  (kv, "session_points_healed");
	stats[session_flags_captured] = KvGetNum  (kv, "session_flags_captured");
	stats[session_custom_wins]    = KvGetNum  (kv, "session_custom_wins");
	stats[session_kill_streak]    = KvGetNum  (kv, "session_kill_streak");
	stats[session_death_streak]   = KvGetNum  (kv, "session_death_streak");
	
	KvGetString(kv, "session_fav_weapon", stats[session_fav_weapon], sizeof(stats[session_fav_weapon]));
	
	// global values
	stats[global_rank]      = KvGetNum  (kv, "global_rank");
	stats[global_players]   = KvGetNum  (kv, "global_players");
	stats[global_kills]     = KvGetNum  (kv, "global_kills");
	stats[global_deaths]    = KvGetNum  (kv, "global_deaths");
	stats[global_kpd]       = KvGetFloat(kv, "global_kpd");
	stats[global_headshots] = KvGetNum  (kv, "global_headshots");
	stats[global_hpk]       = KvGetFloat(kv, "global_hpk");	
}


/// Reads an item from the file cache
WriteStatsToKv(Handle:kv, stats[PlayerStat])
{
	// custom non gameMe values here
	KvSetString(kv, "display_name",   stats[display_name]);
	KvSetString(kv, "steam_id",       stats[steam_id]);
	KvSetNum   (kv, "data_timestamp", stats[data_timestamp]);
	
	// current values
	KvSetNum   (kv, "rank",            stats[rank]);
	KvSetNum   (kv, "players",         stats[players]);	
	KvSetNum   (kv, "skill",           stats[skill]);	
	KvSetNum   (kv, "kills",           stats[kills]);	
	KvSetNum   (kv, "deaths",          stats[deaths]);	
	KvSetFloat (kv, "kpd",             stats[kpd]);
	KvSetNum   (kv, "suicides",        stats[suicides]);
	KvSetNum   (kv, "headshots",       stats[headshots]);
	KvSetFloat (kv, "hpk",             stats[hpk]);
	KvSetFloat (kv, "accuracy",        stats[accuracy]);
	KvSetNum   (kv, "connection_time", stats[connection_time]);
	KvSetNum   (kv, "kill_assists",    stats[kill_assists]);
	KvSetNum   (kv, "kills_assisted",  stats[kills_assisted]);
	KvSetNum   (kv, "points_healed",   stats[points_healed]);
	KvSetNum   (kv, "flags_captured",  stats[flags_captured]);
	KvSetNum   (kv, "custom_wins",     stats[custom_wins]);
	KvSetNum   (kv, "kill_streak",     stats[kill_streak]);
	KvSetNum   (kv, "death_streak",    stats[death_streak]);
	
	// session values
	KvSetNum   (kv, "session_pos_change",     stats[session_pos_change]);
	KvSetNum   (kv, "session_skill_change",   stats[session_skill_change]);
	KvSetNum   (kv, "session_kills",          stats[session_kills]);
	KvSetNum   (kv, "session_deaths",         stats[session_deaths]);
	KvSetFloat (kv, "session_kpd",            stats[session_kpd]);
	KvSetNum   (kv, "session_suicides",       stats[session_suicides]);
	KvSetNum   (kv, "session_headshots",      stats[session_headshots]);
	KvSetFloat (kv, "session_hpk",            stats[session_hpk]);
	KvSetFloat (kv, "session_accuracy",       stats[session_accuracy]);
	KvSetNum   (kv, "session_time",           stats[session_time]);
	KvSetNum   (kv, "session_kill_assists",   stats[session_kill_assists]);
	KvSetNum   (kv, "session_kills_assisted", stats[session_kills_assisted]);
	KvSetNum   (kv, "session_points_healed",  stats[session_points_healed]);
	KvSetNum   (kv, "session_flags_captured", stats[session_flags_captured]);
	KvSetNum   (kv, "session_custom_wins",    stats[session_custom_wins]);
	KvSetNum   (kv, "session_kill_streak",    stats[session_kill_streak]);
	KvSetNum   (kv, "session_death_streak",   stats[session_death_streak]);
	KvSetString(kv, "session_fav_weapon",     stats[session_fav_weapon]);
	
	// global values
	KvSetNum   (kv, "global_rank",      stats[global_rank]);
	KvSetNum   (kv, "global_players",   stats[global_players]);
	KvSetNum   (kv, "global_kills",     stats[global_kills]);
	KvSetNum   (kv, "global_deaths",    stats[global_deaths]);
	KvSetFloat (kv, "global_kpd",       stats[global_kpd]);
	KvSetNum   (kv, "global_headshots", stats[global_headshots]);
	KvSetFloat (kv, "global_hpk",       stats[global_hpk]);	
	
}

/// Clear stale items from file cache
PruneFileCache()
{
	new Handle:kv = GetFileCache();
	
	decl stats[PlayerStat];
	for (new bool:hasItem = KvGotoFirstSubKey(kv); hasItem; hasItem = KvGotoNextKey(kv))
	{
		ReadStatsFromKv(kv, stats);
		// if the stats are older than the limit then don't use them
		if (stats[data_timestamp] + CACHE_PRUNE_LIMIT < GetTime())
		{
			new String:dateString[64];
			ToIsoDateTime(dateString, sizeof(dateString), stats[data_timestamp]);
			
			LogMsg(Format(logMsg, sizeof(logMsg), "Removed stale client data from cache. Name: %s, SteamID: %s, Date: %s", stats[display_name], stats[steam_id], dateString));
			
			// drop it
			KvDeleteThis(kv);
		}
		
		StoreFileCache(kv);
	}
	
	CloseHandle(kv);
}