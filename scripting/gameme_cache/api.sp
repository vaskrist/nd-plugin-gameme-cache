public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
	CreateNative("GetClientGameMeCell",  Native_GetClientGameMeCell);
	CreateNative("GetClientGameMeFloat", Native_GetClientGameMeFloat);
	CreateNative("GetClientGameString",  Native_GetClientGameString);
	
	return APLRes_Success;
}

/// Returns the client's gameMe Cell stat value
public Native_GetClientGameMeCell(Handle:plugin, numParams)
{
	new result;
	
	// retrieve client parameter
	new client = GetNativeCell(1);
	new PlayerStat:key = GetNativeCell(2);
	
	decl stats[PlayerStat];
	if (GetClientStats(client, stats))
	{
		result = stats[key];
	}
	else
	{
		result = -1;
	}
	
	return result;
}

/// Returns the client's gameMe Float stat value
public Native_GetClientGameMeFloat(Handle:plugin, numParams)
{
	new Float:result;
	
	// retrieve client parameter
	new client = GetNativeCell(1);
	new PlayerStat:key = GetNativeCell(2);
	
	decl stats[PlayerStat];
	if (GetClientStats(client, stats))
	{
		result = Float:stats[key];
	}
	else
	{
		result = -1.0;
	}
	
	return _:result;
}

/// Returns the client's gameMe String stat value
public Native_GetClientGameString(Handle:plugin, numParams)
{
	new result;
	
	// retrieve client parameter
	new client = GetNativeCell(1);
	new PlayerStat:key = GetNativeCell(2);
	new len = GetNativeCell(4);
   
	decl stats[PlayerStat];
	if (GetClientStats(client, stats))
	{
		SetNativeString(3, String:stats[key], len, false);	
		result = strlen(String:stats[key]);
	}
	
	return result;
}
