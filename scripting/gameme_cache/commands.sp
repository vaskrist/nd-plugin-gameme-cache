new Handle:cookie_cache_logging = INVALID_HANDLE;

RegisterCommands()
{
	cookie_cache_logging = RegClientCookie("gameMe cache logging On/Off", "", CookieAccess_Protected);
	
	RegAdminCmd("sm_cache_print",       AdminCommand_CachePrint,      ADMFLAG_RESERVATION, "Prints a sorted list of players with their Name, SteamID, rank and skill.");	
	RegAdminCmd("sm_cache_logs_toggle", AdminCommand_CacheLogsToggle, ADMFLAG_RESERVATION, "Toggle log output of the gameMe cache pluing to console on/off for the admin player.");	
	RegAdminCmd("sm_cache_dump",        AdminCommand_CacheDump,       ADMFLAG_RESERVATION, "Dumps an unsorted list list of players with their Name, SteamID, rank and skill.");	
	RegAdminCmd("sm_cache_refresh",     AdminCommand_CacheRefresh,    ADMFLAG_RESERVATION, "Refreshes (re-queries) data for the specified player.");	
}

/// Cookie caching in array
public OnClientCookiesCached(client)
{
	option_cache_logging[client] = GetCookie_cache_logging(client);
}

/// Reading cookies with default being Off
bool:GetCookie_cache_logging(client)
{
	decl String:buffer[10];
	GetClientCookie(client, cookie_cache_logging, buffer, sizeof(buffer));
	
	// if not explicitly set to "On" use "Off" by default
	return StrEqual(buffer, "On");
}

public Action:AdminCommand_CachePrint(client, args)
{
	PrintSortedList(client);
	return Plugin_Handled;
}

public Action:AdminCommand_CacheLogsToggle(client, args)
{
	option_cache_logging[client] = !option_cache_logging[client];
	
	if (option_cache_logging[client])
	{
		SetClientCookie(client, cookie_cache_logging, "On");
		PrintToConsole(client, "\x04gameMe cache logging to console Enabled!");
	}
	else
	{
		SetClientCookie(client, cookie_cache_logging, "Off");
		PrintToConsole(client, "\x04gameMe cache logging to console Disabled!");
	}
	return Plugin_Handled;
}

public Action:AdminCommand_CacheDump(client, args)
{
	DumpWholeList(client);
	return Plugin_Handled;
}

public Action:AdminCommand_CacheRefresh(client, args)
{
	// it could block more calls for this client until we get an answer or it is retrieved - a timestamp on the client data
	if (args < 1)
	{
		PrintToConsole(client, "Usage: sm_cache_refresh <client>");
		return Plugin_Handled;
	}
 
	new String:name[32];
	GetCmdArg(1, name, sizeof(name));
	new cl = StringToInt(name);
	
	ReadClientData(cl);
	return Plugin_Handled;
}

/// Sorts the client index array by skill (would be the same with rank)
public SortFuncNDTopClientsByPoints(index1, index2, Handle:array, Handle:hndl)
{
	decl stats1[PlayerStat];
	decl stats2[PlayerStat];
	
	GetClientStats(GetArrayCell(array, index1), stats1);
	GetClientStats(GetArrayCell(array, index2), stats2);
	
	return compare(stats1[skill], stats2[skill]);
}

/// Prints the sorted list of cached client stats
PrintSortedList(client = 0)
{
	new Handle:sortedClients = CreateArray();
	for (new cl = 0; (cl < MAXPLAYERS); cl++) {
		if (IsValidClient(cl) && infoCache[cl][valid])
		{
			PushArrayCell(sortedClients, cl);
		}
	}
	SortADTArrayCustom(sortedClients, SortFuncNDTopClientsByPoints);

	new count = GetArraySize(sortedClients);
	
	Format(logMsg, sizeof(logMsg), "--- Sorted list of top players (%d) ---", count);
	PrintMessage(client, logMsg);
	decl stats[PlayerStat];
	for (new i = 0; (i < count); i++) {
		new cl = GetArrayCell(sortedClients, i);
		GetClientStats(cl, stats);
		Format(logMsg, sizeof(logMsg), "%d: %s [%s] %d %f", stats[rank], stats[display_name], stats[steam_id], stats[skill], stats[kpd]);
		PrintMessage(client, logMsg);
	}
	Format(logMsg, sizeof(logMsg), "---------------------------------------");
	PrintMessage(client, logMsg);
}

/// Prints the sorted list of cached client stats
DumpWholeList(client = 0)
{
	new count = 0;
	
	Format(logMsg, sizeof(logMsg), "---- Simple list of cached players ----");
	PrintMessage(client, logMsg);
	decl stats[PlayerStat];
	for (new cl = 0; (cl < MAXPLAYERS); cl++) {
		if (IsValidClient(cl) && infoCache[cl][valid])
		{
			GetClientStats(cl, stats);
			Format(logMsg, sizeof(logMsg), "%d: %s [%s] %d %f", stats[rank], stats[display_name], stats[steam_id], stats[skill], stats[kpd]);
			PrintMessage(client, logMsg);
			count++;
		}
	}
	Format(logMsg, sizeof(logMsg), "--------------- (%d) ------------------", count);
	PrintMessage(client, logMsg);
}
