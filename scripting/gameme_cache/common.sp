
PrintMessage(client, String:message[])
{
	if (client) {
		PrintToChat(client, message);
	} else {
		PrintToServer(message);
	}
}

LogMsg(length=0)
{
	LogMessage(logMsg);
	for (new client = 0; client < (MAXPLAYERS + 1) && option_cache_logging[client]; client++)
	{
		PrintToConsole(client, logMsg);
	}
}

LogErr(length=0)
{
	LogError(logMsg);
	for (new client = 0; client < (MAXPLAYERS + 1) && option_cache_logging[client]; client++)
	{
		PrintToConsole(client, logMsg);
	}
}

///
ToIsoDateTime(String:buff[], buffSize, timestamp=-1)
{
	if (timestamp >= -1) {
		FormatTime(buff, buffSize, "%Y-%m-%d %H:%M:%S", timestamp);
	}
	else
	{
		Format(buff, buffSize, "%d", timestamp);
	}
}

/// Returns unique ID - in this case the SteamID
GetUniqueId(client, String:uniqueId[], uniqueIdSize)
{
	GetClientAuthString(client, uniqueId, uniqueIdSize);
	// FIXME: update to 1.6.2
	// GetClientAuthId(client, AuthId_Steam2, uniqueId, uniqueIdSize);
}

/// Checks if the clies is valid a perhaps not a bot
stock bool:IsValidClient(client, bool:nobots = true)
{
	return (client > 0 && client <= MaxClients && IsClientConnected(client) && !(nobots && IsFakeClient(client)) && IsClientInGame(client));
}

compare(value1, value2)
{
	return signum(value1 - value2);
}

signum(value)
{
	return (value > 0) ? 1 : ((value == 0) ? 0 : -1);
}



