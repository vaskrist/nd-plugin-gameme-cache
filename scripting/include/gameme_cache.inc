#if defined _gameme_cache_included
  #endinput
#endif
#define _gameme_cache_included

enum PlayerStat
{
	// current values
	rank,
	players,
	skill,
	kills,
	deaths,
	Float: kpd,
	suicides,
	headshots,
	Float: hpk,
	Float: accuracy,
	connection_time,
	kill_assists,
	kills_assisted,
	points_healed,
	flags_captured,
	custom_wins,
	kill_streak,
	death_streak,
	
	// session values
	session_pos_change,
	session_skill_change,
	session_kills,
	session_deaths,
	Float: session_kpd,
	session_suicides,
	session_headshots,
	Float: session_hpk,
	Float: session_accuracy,
	session_time,
	session_kill_assists,
	session_kills_assisted,
	session_points_healed,
	session_flags_captured,
	session_custom_wins,
	session_kill_streak,
	session_death_streak,
	String: session_fav_weapon[32],
	
	// global values
	global_rank,
	global_players,
	global_kills,
	global_deaths,
	Float: global_kpd,
	global_headshots,
	Float: global_hpk,
	
	// custom non gameMe values here
	String:steam_id[64], // SteamID for caching purposes
	String:display_name[64], // SteamID for caching purposes
	data_timestamp, // time when the data was retrieved from gameMe
	bool:valid, // gets invalidated on player_disconnect
}

/**
 * @brief Returns the client's gameMe Cell stat value.
 *
 * @param client - client index
 * @param key - stat key
 * @return amount of resources
 */
native GetClientGameMeCell(client, PlayerStat:key);

/**
 * @brief Returns the client's gameMe Float stat value.
 *
 * @param client - client index
 * @param key - stat key
 * @return Float value
 */
native Float:GetClientGameMeSkill(client);

/**
 * @brief Returns the client's gameMe String stat value.
 *
 * @param client - client index
 * @param key - stat key
 * @return String value
 */
native Float:GetClientGameMeKdr(client);
