#include <sourcemod>
#include <gameme>
#include <clientprefs>
#include <gameme_cache>
#undef REQUIRE_PLUGIN
#include <updater>

#define GAMEME_CACHE_UPDATE_URL	 "http://vaskrist.source-code.my/nd-plugin-gameme-cache/raw/master/updatefile.txt"
#define GAMEME_CACHE_PLUGIN_URL	 "http://vaskrist.source-code.my/nd-plugin-gameme-cache"
#define GAMEME_CACHE_PLUGIN_NAME    "gameMe cache"
#define GAMEME_CACHE_PLUGIN_VERSION "0.3.5"

/*
* We should keep the info on the players in an array indexed by client in memory for faster access.
* 
* TODO: crate a cache for times when the gameMe site is unavailable
* the cache is possible either using SQLite or KeyValues - SQLite would be difficult to upgrade with new columns
*/

const DAY_IN_MS = 24*60*60*1000;
const CACHE_PRUNE_LIMIT = 90*DAY_IN_MS; // 90 days
const CACHE_STALE_LIMIT = DAY_IN_MS;

new infoCache[MAXPLAYERS][PlayerStat];

// Logging
new bool:option_cache_logging[MAXPLAYERS + 1] = {false,...};
new String:logMsg[1024];

public Plugin:myinfo = 
{
	name =        GAMEME_CACHE_PLUGIN_NAME,
	author =      "Vaskrist",
	description = "Caching GameMe player information for logged on players.",
	version =     GAMEME_CACHE_PLUGIN_VERSION,
	url =         GAMEME_CACHE_PLUGIN_URL
}

#include "gameme_cache/api.sp"
#include "gameme_cache/common.sp"
#include "gameme_cache/commands.sp"
#include "gameme_cache/storage_keyvalue.sp"

public OnLibraryAdded(const String:name[])
{
	if (StrEqual(name, "updater"))
	{
		Updater_AddPlugin(GAMEME_CACHE_UPDATE_URL);
	}
}

public OnPluginStart()
{
	PruneFileCache();
	
	HookEvent("player_disconnect", Event_PlayerDisconnect, EventHookMode_Post);
	
	RegisterCommands();
	
	// IF there are players when the plugin is starting that means it was restarted and needs to get the data anew
	for (new client = 0; client < MaxClients; client++)
	{
		if (IsValidClient(client))
		{
			OnClientCookiesCached(client);
			ReadClientData(client);
		}
	}
	
	if (LibraryExists("updater"))
	{
		Updater_AddPlugin(GAMEME_CACHE_UPDATE_URL);
	}
	
	LogMsg(Format(logMsg, sizeof(logMsg), "Plugin %s started successfully.", GAMEME_CACHE_PLUGIN_NAME));
}

public OnMapStart()
{
	// PrintSortedList();
}

public OnClientAuthorized(client)
{
	if (client > 0) {
		if (!IsFakeClient(client)) {
			ReadClientData(client);
		}
	}
}

public Event_PlayerDisconnect(Handle:event, const String:name[], bool:dontBroadcast)
{
	new userid = GetEventInt(event, "userid");
	new client = GetClientOfUserId(userid);
	
	MarkCacheValid(client, false);

}

/// Reads client data from cache and then requests gameMe current data.
ReadClientData(client)
{
	if (!ReadFromCacheFile(client) || infoCache[client][data_timestamp] < GetTime() - DAY_IN_MS)
	{
		LogMsg(Format(logMsg, sizeof(logMsg), "Calling gameMe for \"playerinfo\" for client %d", client));
		QueryGameMEStats("playerinfo", client, QueryGameMEStatsCallback);
	}
	
}

/// Called when stats have been read from gameMe. Updates custom values in stats, saves the cache to file and sorts the array of client indexes.
StatsRead(client, stats[PlayerStat])
{
	infoCache[client] = stats;
	
	stats[data_timestamp] = GetTime();
	UpdateStats(stats, client);
	MarkCacheValid(client, true);
	
	// TODO: write one or all of them?
	CacheToFile();
}

/// Updates custom data in stats
UpdateStats(stats[PlayerStat], client)
{
	// TODO add level
	GetUniqueId(client, stats[steam_id], sizeof(stats[steam_id]));
	Format(stats[display_name], sizeof(stats[display_name]), "%N", client);
}

/// Marks cache item as (in)valid
MarkCacheValid(client, bool:isValid)
{
	return infoCache[client][valid] = isValid;
}

bool:GetClientStats(client, stats[PlayerStat])
{
	stats = infoCache[client];
	return stats[valid];
}

public QueryGameMEStatsCallback(command, payload, client, &Handle: datapack)
{
	LogMsg(Format(logMsg, sizeof(logMsg), "Callback got called with response from gameMe for client %d with command %d", client, command));
	
	if ((client > 0) && (command == RAW_MESSAGE_CALLBACK_PLAYER)) {
		LogMsg(Format(logMsg, sizeof(logMsg), "Client is > 0 and command correctly 'RAW_MESSAGE_CALLBACK_PLAYER'"));
		
		new Handle: data = CloneHandle(datapack);
		ResetPack(data);
		
		new stats[PlayerStat];
		
		// total values
		stats[rank]            = ReadPackCell(data);
		stats[players]         = ReadPackCell(data);	
		stats[skill]           = ReadPackCell(data);	
		stats[kills]           = ReadPackCell(data);	
		stats[deaths]          = ReadPackCell(data);	
		stats[kpd]             = ReadPackFloat(data);
		stats[suicides]        = ReadPackCell(data);
		stats[headshots]       = ReadPackCell(data);
		stats[hpk]             = ReadPackFloat(data);
		stats[accuracy]        = ReadPackFloat(data);
		stats[connection_time] = ReadPackCell(data);
		stats[kill_assists]    = ReadPackCell(data);
		stats[kills_assisted]  = ReadPackCell(data);
		stats[points_healed]   = ReadPackCell(data);
		stats[flags_captured]  = ReadPackCell(data);
		stats[custom_wins]     = ReadPackCell(data);
		stats[kill_streak]     = ReadPackCell(data);
		stats[death_streak]    = ReadPackCell(data);
		
		// session values
		stats[session_pos_change]     = ReadPackCell(data);
		stats[session_skill_change]   = ReadPackCell(data);
		stats[session_kills]          = ReadPackCell(data);
		stats[session_deaths]         = ReadPackCell(data);
		stats[session_kpd]            = ReadPackFloat(data);
		stats[session_suicides]       = ReadPackCell(data);
		stats[session_headshots]      = ReadPackCell(data);
		stats[session_hpk] 		      = ReadPackFloat(data);
		stats[session_accuracy]       = ReadPackFloat(data);
		stats[session_time]           = ReadPackCell(data);
		stats[session_kill_assists]   = ReadPackCell(data);
		stats[session_kills_assisted] = ReadPackCell(data);
		stats[session_points_healed]  = ReadPackCell(data);
		stats[session_flags_captured] = ReadPackCell(data);
		stats[session_custom_wins]    = ReadPackCell(data);
		stats[session_kill_streak]    = ReadPackCell(data);
		stats[session_death_streak]   = ReadPackCell(data);
		
		ReadPackString(data, stats[session_fav_weapon], sizeof(stats[session_fav_weapon]));
		
		// global values
		stats[global_rank]      = ReadPackCell(data);
		stats[global_players]   = ReadPackCell(data);
		stats[global_kills]     = ReadPackCell(data);
		stats[global_deaths]    = ReadPackCell(data);
		stats[global_kpd]       = ReadPackFloat(data);
		stats[global_headshots] = ReadPackCell(data);
		stats[global_hpk]       = ReadPackFloat(data);
		
		CloseHandle(data);
		
		StatsRead(client, stats);
	}
}
